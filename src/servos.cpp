#include <Arduino.h>

#include <actuators.h>

#include <Servo.h>

Servo servo_door;
Servo servo_window;

void open_door()
{
    servo_door.write(100);
    delay(500);
}

void close_door()
{
    servo_door.write(0);
    delay(500);
}

void open_window()
{
    servo_window.write(140);
    delay(500);
}

void close_window()
{
    servo_window.write(0);
    delay(500);
}