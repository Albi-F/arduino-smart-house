#include <Arduino.h>

#include <actuators.h>
#include <music.h>
#include <sensors.h>
#include <servos.h>

#include <Servo.h>
#include <LiquidCrystal_I2C.h>

volatile int button3state;
volatile int button4state;
String door_key = ".---";
String password;
int password_attempts = 0;

String get_code()
{
  String code;
  String symbol;
  int btn3_press_length = 0;
  while (button4state == 1)
  {
    button3state = digitalRead(button3);
    button4state = digitalRead(button4);

    if (button3state == 0)
    {
      delay(10);
      while (button3state == 0)
      {
        button3state = digitalRead(button3);
        btn3_press_length++;
        Serial.print("100ms");
        delay(100);
      }
      if (btn3_press_length >= 1 && btn3_press_length < 5)
      {
        symbol = ".";
        code = code + symbol;
        Serial.print(code);
        house_display.setCursor(1 - 1, 2 - 1);
        house_display.print(code);
      }
      else if (btn3_press_length >= 5)
      {
        symbol = "-";
        code = code + symbol;
        Serial.print(code);
        house_display.setCursor(1 - 1, 2 - 1);
        house_display.print(code);
      }
    }
    btn3_press_length = 0;
  }
  return code;
}

void door()
{
  button3state = digitalRead(button3);
  button4state = digitalRead(button4);

  // Short press on button 3
  if (button3state == 0)
  {
    password = get_code();
  }

  else if (button4state == 0)
  {
    delay(10);
    int btn4_press_length = 0;

    while (button4state == 0)
    {
      button4state = digitalRead(button4);
      btn4_press_length++;
      delay(100);
    }

    // Short press on button 4
    if (btn4_press_length >= 1 && btn4_press_length < 10)
    {
      if (password == door_key)
      {
        house_display.clear();
        house_display.setCursor(0, 0);
        house_display.print("Welcome home");
        open_door();
        Ode_to_Joy();
        close_door();
      }
      else
      {
        house_display.clear();
        house_display.setCursor(0, 0);
        house_display.print("Error!");
        
        delay(1500);
        password_attempts++;
        if (password_attempts >= 3)
        {
          house_display.clear();
          house_display.setCursor(0, 0);
          tone(buzzer, 440);
          digitalWrite(led1, HIGH);
          house_display.print("System locked");
          delay(4000);
          digitalRead(button4);
          while(button4state == 1){
            button4state = digitalRead(button4);
          }
          noTone(buzzer);
          digitalWrite(led1, LOW);
          password_attempts = 0;
        }
      }
      password = "";
      house_display.clear();
      house_display.setCursor(0, 0);
      house_display.print("password:");
    }

    // Long press on button 4
    else if (btn4_press_length >= 10)
    {
      house_display.clear();
      house_display.print("Edit:");
      door_key = get_code();
      house_display.clear();
      house_display.print("Edited!");
      delay(1500);
      house_display.clear();
      house_display.setCursor(0, 0);
      house_display.print("password:");
    }
    btn4_press_length = 0;
  }
}