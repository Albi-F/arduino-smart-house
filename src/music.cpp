#include <Arduino.h>
#include <actuators.h>

// define name of every sound frequency
#define D0 -1
#define D1 262
#define D2 293
#define D3 329
#define D4 349
#define D5 392
#define D6 440
#define D7 494
#define M1 523
#define M2 586
#define M3 658
#define M4 697
#define M5 783
#define M6 879
#define M7 987
#define H1 1045
#define H2 1171
#define H3 1316
#define H4 1393
#define H5 1563
#define H6 1755
#define H7 1971

#define WHOLE 1
#define HALF 0.5
#define QUARTER 0.25
#define EIGHTH 0.25
#define SIXTEENTH 0.625

// set sound play frequency
int tune[] =
    {
        M3, M3, M4, M5,
        M5, M4, M3, M2,
        M1, M1, M2, M3,
        M3, M2, M2,
        M3, M3, M4, M5,
        M5, M4, M3, M2,
        M1, M1, M2, M3,
        M2, M1, M1,
        M2, M2, M3, M1,
        M2, M3, M4, M3, M1,
        M2, M3, M4, M3, M2,
        M1, M2, D5, D0,
        M3, M3, M4, M5,
        M5, M4, M3, M4, M2,
        M1, M1, M2, M3,
        M2, M1, M1};

// set music beat
float durt[] =
    {
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1 + 0.5,
        0.5,
        1 + 1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1 + 0.5,
        0.5,
        1 + 1,
        1,
        1,
        1,
        1,
        1,
        0.5,
        0.5,
        1,
        1,
        1,
        0.5,
        0.5,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0.5,
        0.5,
        1,
        1,
        1,
        1,
        1 + 0.5,
        0.5,
        1 + 1,
};

void Ode_to_Joy()
{
    int length = sizeof(tune) / sizeof(tune[0]);
    for (int x = 0; x < length; x++)
    {
        tone(buzzer, tune[x]);
        delay(300 * durt[x]);
    }
    noTone(buzzer);
}

void birthday()
{
  tone(buzzer, 294); //digital 3 outputs 294HZ sound 
  delay(250);//delay in 250ms
  tone(buzzer, 440);
  delay(250);
  tone(buzzer, 392);
  delay(250);
  tone(buzzer, 532);
  delay(250);
  tone(buzzer, 494);
  delay(500);
  tone(buzzer, 392);
  delay(250);
  tone(buzzer, 440);
  delay(250);
  tone(buzzer, 392);
  delay(250);
  tone(buzzer, 587);
  delay(250);
  tone(buzzer, 532);
  delay(500);
  tone(buzzer, 392);
  delay(250);
  tone(buzzer, 784);
  delay(250);
  tone(buzzer, 659);
  delay(250);
  tone(buzzer, 532);
  delay(250);
  tone(buzzer, 494);
  delay(250);
  tone(buzzer, 440);
  delay(250);
  tone(buzzer, 698);
  delay(375);
  tone(buzzer, 659);
  delay(250);
  tone(buzzer, 532);
  delay(250);
  tone(buzzer, 587);
  delay(250);
  tone(buzzer, 532);
  delay(500);
}