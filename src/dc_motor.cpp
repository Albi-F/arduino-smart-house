#include <Arduino.h>
#include <actuators.h>

void rotate_dc_motor_clockwise(int8_t speed)
{
  digitalWrite(dc_motor_counterclockwise, LOW);
  digitalWrite(dc_motor_clockwise, speed);
}

void rotate_dc_motor_counterclockwise(int8_t speed)
{
  analogWrite(dc_motor_counterclockwise, speed);
  digitalWrite(dc_motor_clockwise, LOW);
}

void stop_dc_motor()
{
  digitalWrite(dc_motor_counterclockwise, LOW);
  digitalWrite(dc_motor_clockwise, LOW);
}