#include <Arduino.h>

#include <Wire.h>

#include <actuators.h>
#include <dc_motor.h>
#include <entrance.h>
#include <helper_methods.h>
#include <music.h>
#include <sensors.h>
#include <servos.h>

#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C house_display(0x27, 16, 2);

void setup()
{
  Serial.begin(9600);

  house_display.init();
  house_display.backlight();
  // LCD shows "password:" at (column, row)
  house_display.setCursor(0, 0);
  house_display.print("Password:");

  // --- ACTUATORS ---
  // Servo
  servo_door.attach(servo_door_pin);
  servo_window.attach(servo_window_pin);
  close_door();
  close_window();

  // DC motor
  pinMode(dc_motor_clockwise, OUTPUT);
  pinMode(dc_motor_counterclockwise, OUTPUT);

  // LEDs
  pinMode(led1, OUTPUT);
  pinMode(led12, OUTPUT);

  // Others
  pinMode(buzzer, OUTPUT);
  pinMode(relay6, OUTPUT);

  // --- SENSORS ---
  // Analog sensors
  pinMode(gas_sensor10, INPUT);
  pinMode(light_sensor14, INPUT);
  pinMode(steam_sensor13, INPUT);
  pinMode(humidity_sensor, INPUT);

  // Buttons
  pinMode(button3, INPUT);
  pinMode(button4, INPUT);

  // Others
  pinMode(PIRsensor5, INPUT);
}

#define ARRAY_MAX 20
String commands[ARRAY_MAX];
String new_command;
uint8_t read_pointer = 0;
uint8_t write_pointer = 0;

int next_command_index(int current_command_index)
{
  if (current_command_index++ >= ARRAY_MAX)
  {
    return 0;
  }
  else
  {
    return current_command_index++;
  }
}

void serialEvent()
{
  Serial.println("Serial event");
  while (Serial.available())
  {
    Serial.println("Serial available: " + String(Serial.available()) + " bytes");
    // Get the new byte
    char inChar = (char)Serial.read();
    Serial.println("New char received: " + String(inChar));
    new_command += inChar;
    // Check if the incoming character is a newline or a carriage return
    if (inChar == '\n' || inChar == '\r')
    {
      new_command.trim();
      if (new_command.length() == 0)
      {
        return;
      }
      Serial.println("New command received: " + new_command);
      commands[write_pointer] = new_command;
      write_pointer = next_command_index(write_pointer);
      new_command = "";
      Serial.println("Write pointer: " + String(write_pointer));
      Serial.println("Read pointer: " + String(read_pointer));
    }
  }
}

char option;
String secondary_value;

void loop()
{

  door();
  String command_to_execute = commands[read_pointer];
  // Serial.println("Command to execute: " + command_to_execute + ", length: " + command_to_execute.length());
  if (command_to_execute.length() != 0)
  {
    Serial.println("Command to execute: " + command_to_execute + ", length: " + command_to_execute.length());
    option = command_to_execute[0];
    secondary_value = command_to_execute.substring(1);
    commands[read_pointer] = "";
    read_pointer = next_command_index(read_pointer);
    Serial.println("Read pointer: " + String(read_pointer));
  }
  switch (option)
  {

  // Steam sensor
  case 'a':
  {
    uint16_t steam = analogRead(steam_sensor13);
    Serial.print("Steam value: ");
    Serial.println(steam);
    break;
  }

  // Buzzer control
  case 'b':
  {
    if (secondary_value.equals("on"))
    {
      tone(buzzer, 440);
      Serial.println("Buzzer is on");
    }
    else if (secondary_value == String("off"))
    {
      noTone(buzzer);
      Serial.println("Buzzer is off");
    }
    break;
  }

  // Fan control
  case 'f':
  {
    char action = secondary_value[0];
    int speed = secondary_value.substring(1).toInt();
    switch (action)
    {
    case 'c':
    {
      // There is no PWM on this pin, so we can't control the speed
      speed = 255;
      rotate_dc_motor_clockwise(speed);
      Serial.println("Fan is rotating clockwise, speed: " + String(speed));
      break;
    }
    case 'a':
    {
      rotate_dc_motor_counterclockwise(speed);
      Serial.println("Fan is rotating counterclockwise, speed: " + String(speed));
      break;
    }
    case 's':
    {
      stop_dc_motor();
      Serial.println("Fan is stopped");
      break;
    }
    }
    break;
  }

  // Gas sensor
  case 'g':
  {
    uint16_t gas = analogRead(gas_sensor10);
    Serial.println("Gas value: " + String(gas));
    break;
  }

  // Humidity sensor
  case 'h':
  {
    uint16_t humidity = analogRead(humidity_sensor);
    Serial.println("Humidity value: " + String(humidity));
    break;
  }

  // Light sensor
  case 'l':
  {
    uint16_t light = analogRead(light_sensor14);
    Serial.println("Light level: " + String(light));
    break;
  }

  // Relay control
  case 'r':
  {
    if (secondary_value.equals("on"))
    {
      digitalWrite(relay6, HIGH);
      Serial.println("Relay is on");
    }
    else if (secondary_value == String("off"))
    {
      digitalWrite(relay6, LOW);
      Serial.println("Relay is off");
    }
    break;
  }

  // Servo control
  // THIS CASE BREAKS THE PROGRAM
  case 's':
  {
    char object = secondary_value[0];
    String command_value = secondary_value.substring(1);
    bool numeric_value = check_number(command_value);
    if (numeric_value)
    {
      int angle = command_value.toInt();
      switch (object)
      {
      case 'w':
      {
        servo_window.write(angle);
        delay(1000);
        Serial.println("Servo window moved to " + String(angle) + " degrees");
        break;
      }
      case 'd':
      {
        servo_door.write(angle);
        delay(1000);
        Serial.println("Servo door moved to " + String(angle) + " degrees");
        break;
      }
      }
    }
    else
    {
      switch (object)
      {
      case 'w':
      {
        if (command_value.equals("open"))
        {
          open_window();
          Serial.println("Window is opened");
        }
        else if (command_value.equals("close"))
        {
          close_window();
          Serial.println("Window is closed");
        }
        break;
      }
      case 'd':
      {
        if (command_value.equals("open"))
        {
          open_door();
          Serial.println("Door is opened");
        }
        else if (command_value.equals("close"))
        {
          close_door();
          Serial.println("Door is closed");
        }
        break;
      }
      }
    }
    break;
  }

  // White LED control
  case 'w':
  {
    if (secondary_value.equals("on"))
    {
      digitalWrite(led1, HIGH);
      Serial.println("White LED is on");
    }
    else if (secondary_value == String("off"))
    {
      digitalWrite(led1, LOW);
      Serial.println("White LED is off");
    }
    break;
  }

  // Yellow LED control
  case 'y':
  {
    bool numeric_value = check_number(secondary_value);
    if (numeric_value && secondary_value.toInt() >= 0 && secondary_value.toInt() <= 255)
    {
      int brightness = secondary_value.toInt();
      analogWrite(led12, brightness);
      Serial.println("Yellow LED brightness: " + String(brightness));
    }
    else if (secondary_value.equals("on"))
    {
      digitalWrite(led12, HIGH);
      Serial.println("Yellow LED is on");
    }
    else if (secondary_value == String("off"))
    {
      digitalWrite(led12, LOW);
      Serial.println("Yellow LED is off");
    }
    else
    {
      goto INVALID_COMMAND;
    }
    break;
  }

  INVALID_COMMAND:
  default:
  {
    if (option != 0)
    {
      Serial.println("Invalid command: " + String(option) + String(secondary_value));
    }
  }
  }

  option = 0;
  secondary_value = "";
}
