#ifndef actuators_h
#define actuators_h

#include <Servo.h>
#include <LiquidCrystal_I2C.h>

// Set the communication address of I2C to 0x27, display 16 characters every line, two lines in total
extern LiquidCrystal_I2C house_display;

// DC motor pins
#define dc_motor_counterclockwise 6
#define dc_motor_clockwise 7

// Servo pins
extern Servo servo_window;
#define servo_window_pin 10
extern Servo servo_door;
#define servo_door_pin 9

// LEDs
#define led1 13
#define led12 5

// Others
#define relay6 12
#define buzzer 3

#endif