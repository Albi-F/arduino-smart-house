#ifndef servos_h
#define servos_h

void open_door();
void close_door();
void open_window();
void close_window();

#endif