#ifndef sensors_h
#define sensors_h

// --- DIGITAL ---
#define PIRsensor5 2

// Buttons
#define button3 4
#define button4 8

// --- ANALOG ---
#define gas_sensor10 A0
#define light_sensor14 A1
#define steam_sensor13 A2
#define humidity_sensor A3

#endif
