#ifndef dc_motor_h
#define dc_motor_h

void rotate_dc_motor_clockwise(int8_t speed);
void rotate_dc_motor_counterclockwise(int8_t speed);
void stop_dc_motor();

#endif